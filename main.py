import json
from kf_graph import KFGraph
from pprint import pprint


# FILENAME = '60011662-1.0.1.json'
FILENAME = '60011662-1.0.2(1).json'


def is_attr_needed(attr):
    if attr in ['id', 'name', 'type', 'header', 'subHeader', 'buttons', 'components']:
        return True
    return False


def parse_json(fname):
    with open(fname, 'r', encoding='utf-8') as fp:
        data = json.load(fp)
        kfg = KFGraph(data)
        kfg.check_edges()
        # kfg._print_edges()
        # kfg.visualize(show_buttons=True)
        kfg.visualize(show_buttons=False, defaults=True)


if __name__ == '__main__':
    parse_json(FILENAME)
