from dataclasses import dataclass
from pprint import pprint, pformat
from pyvis.network import Network
import networkx as nx


@dataclass
class KFNode:
    id: int
    kf_id: str
    name: str
    type: str
    header: str
    sub_header: str
    components: [str]


@dataclass
class KFEdge:
    node_from: KFNode
    node_to: KFNode
    conditions: [dict]  # {'field': 'value') - зависимость/и от значений компонентов
    color = None  # цвет ребер


@dataclass
class KFComponent:
    id: str
    name: str
    type: str
    label: str
    answers_values: [str]  # возможные значения ответов ('value': '1', 'value': '0') или пустой список []
    dictionary = ''


class KFGraph:
    def __init__(self, json_data):
        # получаем ноды/вершины графа (экраны)
        self._graph = None
        self._nodes = []
        for screen in json_data['screens']:
            node = KFNode(id=int((screen['id']).replace('s', '')), kf_id=screen['id'], name=screen['name'],
                          type=screen['type'], header=screen['header'], sub_header=screen['subHeader'],
                          components=screen['components'])
            self._nodes.append(node)

        # получаем рёбра графа (правила перехода)
        self._edges = []
        # pprint(json_data['screenRules'].keys(), indent=2)
        for key in json_data['screenRules'].keys():
            # print(key)
            # print(json_data['screenRules'][key])

            for conditions in json_data['screenRules'][key]:
                # print('='*20)
                # print(key, '===>', conditions['nextDisplay'])
                # pprint(conditions['conditions'])

                edge = KFEdge(node_from=self._search_node(key), node_to=self._search_node(conditions['nextDisplay']),
                              conditions=conditions['conditions'])
                self._edges.append(edge)
                # print(edge.conditions)

        # Список компонентов, возможных их значений, словарей (при наличии)
        self._components = []
        for component in json_data['applicationFields']:
            answers_values = []
            attrs = component.get('attrs', False)
            if not attrs: continue  # нет аттрибутов, пропускаем

            # вариант 1
            attrs_answers = attrs.get('answers', False)
            if attrs_answers:
                for variant in attrs_answers:
                    answers_values.append(variant['value'])

            # вариант 2
            attrs_supported_values = attrs.get('supportedValues')
            if attrs_supported_values:
                for variant in attrs_supported_values:
                    answers_values.append(variant['value'])

            # создаём компонент из того, что стало известно
            kf_component = KFComponent(id=component['id'], name=component['name'], type=component['type'],
                                       label=component['label'], answers_values=answers_values)
            # есть ли словарь? Добавляем аттрибут
            dictionary = attrs.get('dictionaryType', False)
            if dictionary:
                kf_component.dictionary = dictionary

            # пополняем список компонентов
            self._components.append(kf_component)

    def _search_node(self, name):
        # Ищем ноду по имени
        for node in self._nodes:
            if node.kf_id == name:
                return node
        print(f'Не нашёл ноду с именем {name} в списке нод/вершин/экранов')

    def _search_component(self, name):
        # Ищем компонент по имени
        for component in self._components:
            if component.id == name:
                return component
        print(f'Не нашёл компонент с именем {name} в списке компонентов')

    def _print_edges(self):
        # список ребёр и условий
        for e in self._edges:
            if e.node_to.kf_id == 's114':
                print(f'{e.node_from.kf_id} => {e.node_to.kf_id} || {e.conditions}')

    def check_edges(self):
        # Проверяет условия на рёбрах значениям из компонентов
        for edge in self._edges:
            for condition in edge.conditions:
                if condition['value'] is not None:  # отбрасываем нулевые значения условий
                    # print('='*50)
                    # print(condition)
                    # ищем компонент по имени
                    for component in self._components:
                        if component.id == condition['field']:
                            # print(component)
                            if condition['value'] not in component.answers_values:
                                print('=' * 50, 'Возможная ошибка в условии', '=' * 50)
                                print(f'Переход: {edge.node_from.kf_id} => {edge.node_to.kf_id} '
                                      f'({edge.node_from.name} => {edge.node_to.name})')
                                print(f'Условия перехода: {edge.conditions}')
                                print(f'Зависимость от значения компонента: {component}')
                                print(f'Компонент: {condition["field"]}')
                                print()
                                # красим ребро
                                edge.color = 'red'
                else:
                    if condition['args'] is not None:
                        print('=' * 20, 'Выбор из значений справочника, нужно сверить наличие значения и посмотреть, '
                                        'все ли нужные значения имеются в вариантах', '=' * 20)
                        print(f'Переход: {edge.node_from.kf_id} => {edge.node_to.kf_id} '
                              f'({edge.node_from.name} => {edge.node_to.name})')

                        component = self._search_component(condition['field'].replace('.value.id', ''))
                        # print(component)
                        print(f'Справочник: {component.dictionary} из компонента {component.id} ({component.name})')

                        # print(condition)
                        field = condition['field']
                        predicate = condition['predicate']
                        values = [a['value'] for a in condition['args']]
                        print(f'Условие: {field}, предикат: {predicate}, значения: {values}')
                        print()
                        # красим ребро
                        edge.color = 'magenta'

    def visualize(self, show_buttons=False, defaults=False):
        """
        Запустить браузер с графом
        :param show_buttons: показывать настройки visjs
        :param defaults: использовать настройки visjs по умолчанию (предыдущий парамет игнорируется)
        :return:
        """
        self._graph = nx.DiGraph()
        for n in self._nodes:  # добавляем вершины
            self._graph.add_node(n.id, title=f'{n.name}\n{n.header}', label=n.kf_id)
        for edge in self._edges:  # добавляем ноды
            title = pformat(edge.conditions, compact=False)  # к сожалению, html не разрешён :(
            lbl = []  # на всякий случай
            for condition in edge.conditions:
                if condition['value'] is not None:  # отбрасываем нулевые значения условий
                    lbl.append(condition['value'])  # наступил всякий случай, таки массив

                else:
                    if condition['args'] is not None:
                        lbl = [a['value'] for a in condition['args']]  # сразу массив, всякий случай не наступил
                        component = self._search_component(condition['field'].replace('.value.id', ''))
                        # print(component)
                        title = f'Справочник: {component.dictionary} из компонента {component.id} ' \
                              f'({component.name})\n\n{title}'
            self._graph.add_edge(edge.node_from.id, edge.node_to.id, title=title, label=str(lbl), color=edge.color)

        # тут можно размер холста поставить, и выключить выделение
        nt = Network('900px', '100%', directed=True, select_menu=True)
        if not defaults:
            if show_buttons:
                nt.show_buttons()
            else:  # когда-то будут более удобные по умолчанию опции
                nt.set_options("""var options = {
          "edges": {
            "color": {
              "inherit": true
            },
            "smooth": false
          },
          "layout": {
            "hierarchical": {
              "enabled": true,
              "levelSeparation": 75,
              "nodeSpacing": 40,
              "direction": "LR",
              "sortMethod": "hubsize",
              "edgeMinimization": true
            }
          },
          "physics": {
            "hierarchicalRepulsion": {
              "centralGravity": 0
            },
            "minVelocity": 0.75,
            "solver": "hierarchicalRepulsion"
          }
        }""")
        nt.from_nx(self._graph)
        nt.show('nx.html', local=False)
