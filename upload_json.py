import requests
import json

SERVICE = '60013743'
VERSION = '1.0.0'
COOKIE_TOKEN = 'eyJ2ZXIiOjEsInR5cCI6IkpXVCIsInNidCI6ImFjY2VzcyIsImFsZyI6IlJTMjU2In0.eyJuYmYiOjE2Nzk1NjE2ODMsInNjb3BlIjoiaHR0cDpcL1wvZXNpYS5nb3N1c2x1Z2kucnVcL29yZ19pbnZ0cz9vcmdfb2lkPTEwMDAxMzUzNDYmbW9kZT13IGh0dHA6XC9cL2VzaWEuZ29zdXNsdWdpLnJ1XC9vcmdfdmhscz9vcmdfb2lkPTEwMDAxMzUzNDYmbW9kZT13IGh0dHA6XC9cL2VzaWEuZ29zdXNsdWdpLnJ1XC9vcmdfcHJvZmlsZT9vcmdfb2lkPTEwMDAxMzUzNDYgaHR0cDpcL1wvZXNpYS5nb3N1c2x1Z2kucnVcL3Vzcl9pbmY_bW9kZT13Jm9pZD0yNDE5MDEzODEgaHR0cDpcL1wvZXNpYS5nb3N1c2x1Z2kucnVcL29yZ19lbXBzP29yZ19vaWQ9MTAwMDEzNTM0NiZtb2RlPXcgaHR0cDpcL1wvZXNpYS5nb3N1c2x1Z2kucnVcL29yZ19pbmY_b3JnX29pZD0xMDAwMTM1MzQ2Jm1vZGU9dyBodHRwOlwvXC9lc2lhLmdvc3VzbHVnaS5ydVwvb3JnX2N0dHM_b3JnX29pZD0xMDAwMTM1MzQ2Jm1vZGU9dyIsImlzcyI6Imh0dHA6XC9cL2VzaWEuZ29zdXNsdWdpLnJ1XC8iLCJ1cm46ZXNpYTpzaWQiOiI0YTlhZDBkYi0yNDViLTY3MTItNjIzZi1mYTA5ZTkyNWIzNjMiLCJ1cm46ZXNpYTpzYmpfaWQiOjI0MTkwMTM4MSwiZXhwIjoxNjc5NTcyNDgzLCJpYXQiOjE2Nzk1NjE2ODMsImNsaWVudF9pZCI6IlBHVSJ9.A7WJf2Wd2g2ZSbcq8HNHdYKbkI2TbVJJUsnOJfHTGrpyB-crFhNEw5UUmlGo2Qj-4MsiFkXhRRIvK-QRaAHDiveqXYnEe3DPe5lPR-WrPXysQzHXcIPqkngg25E-GvkU74qHWIccn5JsEE5h_uLHkRmh-qcRIpp9SLGihz8pcQpG0_nSvNz-5O3yeGp8i9MI5uw8XFkzdH3LusZR6QJGnwgHbu0aroWBjVmb0tCPOfQc3noGKISI36YllSIjldEjH16hKWMpPLqCNUp9iCf9K4ihz6hz_kLwey3iZctyUlXHjE_fuCikVzKYsD5OqDTNRdSi85FbN8o5rF83UAKs0Q'
URL = f'https://vku.test.gosuslugi.ru/api/service-lib/v1/service/{SERVICE}/version/{VERSION}/commit'
JSON_FILE = '60009973-1.0.3.json'

if __name__ == '__main__':
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json, text/plain, */*',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36'
    }
    cookies = {
        'acc_t': COOKIE_TOKEN,
    }

    crafted_data = json.load(open('commit_payload.json', encoding='utf-8'))

    import_data = json.load(open(JSON_FILE, encoding='utf-8'))
    #
    crafted_data['data']['screens'] = import_data['screens']
    crafted_data['data']['components'] = import_data['applicationFields']
    crafted_data['data']['screenRules'] = import_data['screenRules']
    #
    r = requests.post(URL, cookies=cookies, headers=headers, json=crafted_data)
    print(r.text)