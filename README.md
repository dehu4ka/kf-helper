### Запуск

#### установка зависимостей
Из venv в корне проекта выполнить:
```shell
pip install requirements.txt
```

#### запуск
```shell
python main.py
```